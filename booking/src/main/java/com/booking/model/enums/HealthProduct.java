package com.booking.model.enums;

/**
 * Hold constants for Health products
 */
public enum HealthProduct {

    PAIN_RELIEF,
    ANTIBIOTIC,
    INJECTION_THERAPY,
    IBUPROFEN,
    ACETAMINOPHEN,
    TRAMADOL_HCL,
    OXYCODONE_HCL,
    PERCOC,
    VICODIN,
    NORCO,
    NAPROX,
    NAPROXE,
    NULL
}
