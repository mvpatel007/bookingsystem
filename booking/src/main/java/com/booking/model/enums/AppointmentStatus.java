package com.booking.model.enums;

/**
 * Hold constants for Appointment status
 */
public enum AppointmentStatus {
    BOOKED,
    CANCELED,
    MISSED,
    ATTENDED,
}
