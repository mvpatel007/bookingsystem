package com.booking.model.enums;

/**
 * Hold constants for Health Service
 */
public enum HealthService {

    MOBILISATION_OF_SPINE,
    MOBILISATION_OF_JOIN,
    NEURAL_MOBILISATION,
    ACUPUNCTURE,
    MASSAGE,
    JOINT_MOBILISATION,
    JOINT_MANIPULATION,
    INSTRUMENT_MOBILISATION,
    MIN_ENERGY_TECHNIQUES,
    MUSCLE_STRETCHING,
    NEURODYNAMICS,
}
