package com.booking.model.dao;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Responsible for all data access for physiotherapist
 * Responsible for all data manipulation for physiotherapist
 */
public class Physiotherapist {
    private static final AtomicInteger count = new AtomicInteger(1001);
    private final int physiotherapistId;
    String name;

    public Physiotherapist(String name) {
        physiotherapistId = count.getAndIncrement();
        this.name = name;
    }

    public int getPhysiotherapistId() {
        return physiotherapistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
