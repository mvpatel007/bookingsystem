package com.booking.model.dao;


import com.booking.model.enums.HealthProduct;

/**
 * Responsible for data access for physiotherapist
 * Responsible for data manipulation for physiotherapist
 */
public class QuantityHealthProduct {
    HealthProduct healthProduct;
    int quantity;

    public QuantityHealthProduct(HealthProduct healthProduct, int quantity) {
        this.healthProduct = healthProduct;
        this.quantity = quantity;
    }

    public HealthProduct getHealthProduct() {
        return healthProduct;
    }

    public void setHealthProduct(HealthProduct healthProduct) {
        this.healthProduct = healthProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
