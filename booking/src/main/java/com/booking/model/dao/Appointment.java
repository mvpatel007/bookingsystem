package com.booking.model.dao;

import com.booking.model.enums.AppointmentStatus;
import com.booking.model.enums.HealthService;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Responsible for all data access for appointment
 * Responsible for all data manipulation for appointment
 */
public class Appointment {

    private static final AtomicInteger count = new AtomicInteger(3001);
    private final int appointmentId;
    private PatientRecord patientRecord;
    private LocalDateTime dateTime;
    private HealthService healthService;
    private AppointmentStatus appointmentStatus;

    public Appointment(PatientRecord patientRecord, LocalDateTime dateTime, HealthService healthService, AppointmentStatus appointmentStatus) {
        this.appointmentId = count.getAndIncrement();
        this.patientRecord = patientRecord;
        this.dateTime = dateTime;
        this.healthService = healthService;
        this.appointmentStatus = appointmentStatus;
    }

    public static AtomicInteger getCount() {
        return count;
    }

    public int getAppointmentId() {
        return appointmentId;
    }

    public PatientRecord getPatientRecord() {
        return patientRecord;
    }

    public void setPatientRecord(PatientRecord patientRecord) {
        this.patientRecord = patientRecord;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public HealthService getHealthService() {
        return healthService;
    }

    public void setHealthService(HealthService healthService) {
        this.healthService = healthService;
    }

    public AppointmentStatus getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }
}
