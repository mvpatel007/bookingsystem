package com.booking.model.dao;

import com.booking.model.enums.HealthProduct;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Responsible for all data access for patient record
 * Responsible for all data manipulation for patient record
 */
public class PatientRecord {

    private static final AtomicInteger count = new AtomicInteger(2001);
    private final int patientId;
    private String name;
    private String contactNumber;
    private String note;
    private Physiotherapist physiotherapist;
    private HealthProduct healthProduct;

    public PatientRecord(String name, String contactNumber, String note, Physiotherapist physiotherapist, HealthProduct healthProduct) {
        patientId = count.getAndIncrement();
        this.name = name;
        this.contactNumber = contactNumber;
        this.note = note;
        this.physiotherapist = physiotherapist;
        this.healthProduct = healthProduct;
    }

    public int getPatientId() {
        return patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Physiotherapist getPhysiotherapist() {
        return physiotherapist;
    }

    public void setPhysiotherapist(Physiotherapist physiotherapist) {
        this.physiotherapist = physiotherapist;
    }

    public HealthProduct getHealthProduct() {
        return healthProduct;
    }

    public void setHealthProduct(HealthProduct healthProduct) {
        this.healthProduct = healthProduct;
    }
}
