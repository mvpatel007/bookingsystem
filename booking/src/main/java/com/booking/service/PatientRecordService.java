package com.booking.service;


import com.booking.model.dao.PatientRecord;

import java.util.Map;

/**
 *
 */
public interface PatientRecordService {

    /**
     * Add Patient
     *
     * @param patientRecord
     */
    void addPatientRecord(PatientRecord patientRecord);

    /**
     * Set ordered Health product
     */
    void setOrderedHealthProduct();

    /**
     * Display ordered health product
     */
    void displayOrderedHealthProduct();

    /**
     * Display current health product
     */
    void displayCurrentHealthProduct();

    /**
     * display patient record
     *
     * @param patientRecord
     */
    void displayPatientRecord(PatientRecord patientRecord);

    /**
     * Get patient record map
     *
     * @return
     */
    Map<Integer, PatientRecord> getPatientRecordMap();

    /**
     * get patient record by name and number
     *
     * @param name
     * @param contactNumber
     * @return
     * @throws Exception
     */
    PatientRecord getPatientRecordByNameAndNumber(String name, String contactNumber) throws Exception;

}
