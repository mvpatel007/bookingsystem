package com.booking.service.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/**
 * DateUtils
 */
public class DateUtils {

    /**
     * Get date by scanner
     *
     * @param scanner
     * @return
     */
    public static LocalDateTime getDateByScanner(Scanner scanner) {
        try {
            System.out.println("Give a future date & time, i.e. 01/03/2019 11:00 or 01/04/2019 10:30");
            String scannerFullDate = scanner.next();
            String scannerTime = scanner.next();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
            return LocalDateTime.parse(scannerFullDate + " " + scannerTime, formatter);

        } catch (IllegalArgumentException | DateTimeParseException | NullPointerException e) {
            System.out.println(e.getMessage());
            return getDateByScanner(scanner);
        }
    }

}