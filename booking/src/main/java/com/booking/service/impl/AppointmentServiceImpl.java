package com.booking.service.impl;

import com.booking.model.dao.Appointment;
import com.booking.model.dao.PatientRecord;
import com.booking.model.dao.Physiotherapist;
import com.booking.model.enums.AppointmentStatus;
import com.booking.model.enums.HealthService;
import com.booking.service.AppointmentService;
import com.booking.service.PatientRecordService;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * All appointment related service will be in this class
 * This class has implemented interface AppointmentService
 */
public class AppointmentServiceImpl implements AppointmentService {

    private Map<Integer, Appointment> appointmentMap = new HashMap<>();

    /**
     * Add Appointment
     *
     * @param appointment
     */
    @Override
    public void addAppointment(Appointment appointment) {
        appointmentMap.put(appointment.getAppointmentId(), appointment);
    }

    /**
     * Book Appointment
     *
     * @param patientRecord   - Object of patientRecord class
     * @param appointmentDate
     * @param healthService
     * @throws Exception
     */
    @Override
    public void bookAppointment(PatientRecord patientRecord, LocalDateTime appointmentDate, HealthService healthService) throws Exception {

        if (Objects.nonNull(patientRecord)) {
            Physiotherapist physiotherapist = patientRecord.getPhysiotherapist();
            if (isAppointmentDateAvailable(appointmentDate, physiotherapist.getPhysiotherapistId())) {
                Appointment appointment = new Appointment(patientRecord, appointmentDate, healthService, AppointmentStatus.BOOKED);
                addAppointment(appointment);

                System.out.println("Appointment is booked successfully check details below\n");
                displayAppointment(appointment);
            } else {
                throw new Exception("Given date is not available or not valid");
            }
        } else {
            throw new Exception("Given Patient id is not valid");
        }
    }


    /**
     * Change appointment date
     *
     * @param appointment
     * @param appointmentDate
     */
    @Override
    public void changeAppointmentDate(Appointment appointment, LocalDateTime appointmentDate) {
        try {
            if (appointment != null) {
                if (isAppointmentDateAvailable(appointmentDate, appointment.getPatientRecord().getPhysiotherapist().getPhysiotherapistId())) {
                    appointment.setDateTime(appointmentDate);
                    System.out.println("Date and time is updated successfully for given appointment, check updated appointment below\n");
                    displayAppointment(appointment);
                } else {
                    throw new Exception("Given date is not available or not valid");
                }
            } else {
                throw new Exception("Given Appointment id is not valid");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Cancel Appointment
     *
     * @param appointment
     * @throws Exception
     */
    @Override
    public void cancelAppointment(Appointment appointment) throws Exception {

        if (appointment != null) {
            if (appointment.getAppointmentStatus() == AppointmentStatus.BOOKED) {
                appointment.setAppointmentStatus(AppointmentStatus.CANCELED);
                System.out.println("Selected Appointment is cancelled successfully please check details below \n");
                displayAppointment(appointment);
            } else {
                throw new Exception("Selected Appointment is past or already cancelled");
            }
        }
    }

    /**
     * Print Table row border for make better looks
     *
     * @param input
     * @param limit
     */
    private void printTableRowBorder(String input, int limit) {
        System.out.printf("+");
        Stream.generate(() -> input).limit(limit).forEach(System.out::print);
        System.out.println("+");
    }

    /**
     * Print table header for better looks
     *
     * @param heading
     */
    private void printTableHeaderForAppointmentList(String heading) {
        printTableRowBorder("=", 100);
        System.out.printf("|%-100s|\n", heading);
        printTableRowBorder("-", 100);
        System.out.printf("|%-12s| %-14s| %-15s| %-16s| %-23s| %-10s|\n", "Patient Name", "Appointment Id", "Physiotherapist", "Date & Time", "Health Service", "Status");
        printTableRowBorder("=", 100);
    }

    /**
     * Print Table header for count appointment by status
     */
    private void printTableHeaderForCountAppointmentByStatus() {
        printTableRowBorder("=", 44);
        System.out.println("|Record of all appointments in the last month|");
        printTableRowBorder("-", 44);
        System.out.printf("| %-19s|  %-21s| %1s", "Status", "Total", "\n");
        printTableRowBorder("=", 44);
    }

    /**
     * display last month appointments
     *
     * @param patientRecordService
     * @throws Exception
     */
    @Override
    public void displayLastMonthAppointments(PatientRecordService patientRecordService) throws Exception {

        printTableHeaderForAppointmentList("Record of last months appointments for each patient");
        int totalAppointment = 0;
        int bookedAppointment = 0;
        int cancelledAppointment = 0;
        int missedAppointment = 0;
        int attendedAppointment = 0;

        for (PatientRecord patientRecord : patientRecordService.getPatientRecordMap().values()) {

            int appointmentNumber = 1;
            for (Appointment appointment : appointmentMap.values()) {

                if (appointment.getPatientRecord() == patientRecord
                        && appointment.getDateTime().getMonthValue() < LocalDateTime.now().getMonthValue()) {

                    switch (appointment.getAppointmentStatus()) {
                        case BOOKED:
                            bookedAppointment++;
                            break;
                        case CANCELED:
                            cancelledAppointment++;
                            break;
                        case MISSED:
                            missedAppointment++;
                            break;
                        case ATTENDED:
                            attendedAppointment++;
                            break;
                    }

                    String patientName = "";
                    if (appointmentNumber == 1) {
                        patientName = patientRecord.getName();
                    }

                    System.out.printf("|%-12s| %-14s| %-15s| %-16s| %-23s| %-10s|\n",
                            patientName,
                            appointment.getAppointmentId(),
                            patientRecord.getPhysiotherapist().getName(),
                            appointment.getDateTime(),
                            appointment.getHealthService(),
                            appointment.getAppointmentStatus());
                    totalAppointment++;
                    appointmentNumber++;
                }
            }
            if (appointmentNumber > 1) {
                printTableRowBorder("-", 100);
            }
        }
        printTableRowBorder("=", 100);
        System.out.println();
        printTableHeaderForCountAppointmentByStatus();
        System.out.printf("| %-19s|      %-17d| %s", "Booked", bookedAppointment, "\n");
        printTableRowBorder("-", 44);
        System.out.printf("| %-19s|      %-17d| %s", "Cancelled", cancelledAppointment, "\n");
        printTableRowBorder("-", 44);
        System.out.printf("| %-19s|      %-17d| %s", "Missed", missedAppointment, "\n");
        printTableRowBorder("-", 44);
        System.out.printf("| %-19s|      %-17d| %s", "Attended", attendedAppointment, "\n");
        printTableRowBorder("-", 44);
        System.out.printf("| %-19s|      %-17d| %s", "Total Appointments", totalAppointment, "\n");
        printTableRowBorder("-", 44);
        System.out.println();
//        System.out.println(display);
    }

    /**
     * display appointment
     *
     * @param appointment
     */
    @Override
    public void displayAppointment(Appointment appointment) {

        PatientRecord patientRecord = appointment.getPatientRecord();
        printTableHeaderForAppointmentList("Appointment Details");
        displayAppointmentEachPatient(appointment, true);
        printTableRowBorder("=", 100);
    }

    /**
     * Display appointment by each patient
     *
     * @param appointment
     * @param displayPatientName
     */
    private void displayAppointmentEachPatient(Appointment appointment, Boolean displayPatientName) {

        String patientName = "";
        if (displayPatientName) {
            patientName = appointment.getPatientRecord().getName();
        }
        System.out.printf("|%-12s| %-14s| %-15s| %-16s| %-23s| %-10s|\n",
                patientName,
                appointment.getAppointmentId(),
                appointment.getPatientRecord().getPhysiotherapist().getName(),
                appointment.getDateTime(),
                appointment.getHealthService(),
                appointment.getAppointmentStatus());
    }

    /**
     * get Appointment by appointment id
     *
     * @param appointmentId
     * @return
     */
    @Override
    public Appointment getAppointment(int appointmentId) {
        return appointmentMap.get(appointmentId);
    }

    /**
     * display appointment by patient record
     *
     * @param patientRecord
     */
    @Override
    public void displayAppointmentByPatientRecord(PatientRecord patientRecord) {

        int appointmentCount = 1;
        Boolean displayPatientName = true;
        String patientName = "";
        printTableHeaderForAppointmentList("Appointment Details");

        for (Appointment appointment : appointmentMap.values()) {

            if (patientRecord.equals(appointment.getPatientRecord())) {
                if (appointmentCount != 1) {
                    displayPatientName = false;
                }
                displayAppointmentEachPatient(appointment, displayPatientName);
                printTableRowBorder("-", 100);
                appointmentCount++;
            }
        }
        printTableRowBorder("=", 100);
    }

    /**
     * Check that is appointment date is available or not
     *
     * @param appointmentDate
     * @param physiotherapistId
     * @return
     */
    @Override
    public Boolean isAppointmentDateAvailable(LocalDateTime appointmentDate, int physiotherapistId) {
        try {
            boolean result = true;
            if (((appointmentDate.getMinute() != 30 && appointmentDate.getMinute() != 0))
                    || (appointmentDate.getHour() < 7 || appointmentDate.getHour() > 22)
            ) {
                result = false;
            } else {
                for (Appointment appointment : appointmentMap.values()) {
                    if (!appointmentDate.isAfter(LocalDateTime.now())
                            || (appointment.getDateTime().equals(appointmentDate)
                            && appointment.getPatientRecord().getPhysiotherapist().getPhysiotherapistId() == physiotherapistId)) {
                        result = false;
                    }
                }
            }
            return result;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return isAppointmentDateAvailable(appointmentDate, physiotherapistId);
        }
    }

    /**
     * Check in process
     *
     * @param appointment
     * @return
     */
    @Override
    public Boolean checkInPatient(Appointment appointment) {

        Boolean result = false;

        if (appointment.getAppointmentStatus() == AppointmentStatus.BOOKED) {
            appointment.setAppointmentStatus(AppointmentStatus.ATTENDED);
            result = true;
        }
        return result;
    }
}
