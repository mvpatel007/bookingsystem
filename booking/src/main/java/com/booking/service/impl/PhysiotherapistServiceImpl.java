package com.booking.service.impl;

import com.booking.model.dao.Physiotherapist;
import com.booking.service.PhysiotherapistService;

import java.util.HashMap;
import java.util.Map;

/**
 * Services for physiotherapist
 */
public class PhysiotherapistServiceImpl implements PhysiotherapistService {

    Map<Integer, Physiotherapist> physiotherapistMap = new HashMap<>();

    /**
     * Add physiotherapist
     *
     * @param physiotherapist - physiotherapist pbject
     * @throws Exception
     */
    @Override
    public void addPhysiotherapist(Physiotherapist physiotherapist) throws Exception {
        try {
            physiotherapistMap.put(physiotherapist.getPhysiotherapistId(), physiotherapist);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    /**
     * Get physiotherapist object
     *
     * @param physiotherapistId
     * @return
     * @throws Exception
     */
    @Override
    public Physiotherapist getPhysiotherapist(int physiotherapistId) throws Exception {
        try {
            return physiotherapistMap.get(physiotherapistId);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
