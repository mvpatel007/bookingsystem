package com.booking.service.impl;


import com.booking.model.dao.PatientRecord;
import com.booking.model.dao.QuantityHealthProduct;
import com.booking.model.enums.HealthProduct;
import com.booking.service.PatientRecordService;

import java.util.*;
import java.util.stream.Stream;

/**
 * Servcies for Patient record
 */
public class PatientRecordServiceImpl implements PatientRecordService {

    private Map<Integer, PatientRecord> patientRecordMap = new HashMap<>();
    private List<QuantityHealthProduct> quantityHealthProductList;

    /**
     * Add Patient Record
     *
     * @param patientRecord
     */
    @Override
    public void addPatientRecord(PatientRecord patientRecord) {
        patientRecordMap.put(patientRecord.getPatientId(), patientRecord);
    }

    /**
     * Set ordered Health product
     */
    @Override
    public void setOrderedHealthProduct() {

        quantityHealthProductList = new ArrayList<>();

        for (PatientRecord patientRecord : patientRecordMap.values()) {
            if (patientRecord.getHealthProduct() != HealthProduct.NULL) {
                boolean quantityAdded = false;
                for (QuantityHealthProduct eachQuantityHealthProduct : quantityHealthProductList) {
                    if (eachQuantityHealthProduct.getHealthProduct() == patientRecord.getHealthProduct()) {
                        eachQuantityHealthProduct.setQuantity(eachQuantityHealthProduct.getQuantity() + 1);
                        quantityAdded = true;
                        break;
                    }
                }

                if (quantityAdded == false) {
                    QuantityHealthProduct quantityHealthProduct = new QuantityHealthProduct(patientRecord.getHealthProduct(), 1);
                    quantityHealthProductList.add(quantityHealthProduct);
                }

            }
        }
    }

    /**
     * Print table row border
     *
     * @param input
     * @param limit
     */
    private void printTableRowBorder(String input, int limit) {
        System.out.printf("+");
        Stream.generate(() -> input).limit(limit).forEach(System.out::print);
        System.out.println("+");
    }

    /**
     * Print table header for ordered health product
     */
    private void printTableHeaderForOrdererdHealthProduct() {
        printTableRowBorder("=", 35);
        System.out.printf("|%-35s|\n", "Ordered Health products");
        printTableRowBorder("-", 35);
        System.out.printf("|%-25s| %-8s|\n", "Health Product", "Quantity");
        printTableRowBorder("=", 35);
    }

    /**
     * Print table header for each patient record
     */
    private void printTableHeaderForEachPatientRecord() {
        printTableRowBorder("=", 39);
        System.out.printf("|%-39s|\n", "Health Product for each patient");
        printTableRowBorder("-", 39);
        System.out.printf("|%-12s| %-25s|\n", "Patient Name", "Health Product");
        printTableRowBorder("=", 39);
    }

    /**
     * Print Table header fro last updated patient record
     */
    private void printTableHeaderForLastUpdatedPatientRecord() {
        printTableRowBorder("=", 116);
        System.out.printf("|%-116s|\n", "Last updated patient record");
        printTableRowBorder("-", 116);
        System.out.printf("|%-12s| %-14s| %-15s| %-17s| %-50s|\n",
                "Patient Name",
                "Contact Number",
                "Physiotherapist",
                "Health Product",
                "Note");
        printTableRowBorder("=", 116);
    }


    /**
     * Display Ordered health product
     */
    @Override
    public void displayOrderedHealthProduct() {
        setOrderedHealthProduct();

        if (quantityHealthProductList.size() == 0) {
            System.out.println("No Health product is ordered.");
        } else {
            printTableHeaderForOrdererdHealthProduct();

            for (QuantityHealthProduct eachQuantityHealthProduct : quantityHealthProductList) {

                System.out.printf("|%-25s| %-8s|\n",
                        eachQuantityHealthProduct.getHealthProduct(),
                        eachQuantityHealthProduct.getQuantity());
                printTableRowBorder("-", 35);
            }
            printTableRowBorder("=", 35);
        }

    }

    /**
     * Display current health product
     */
    @Override
    public void displayCurrentHealthProduct() {
        printTableHeaderForEachPatientRecord();
        for (PatientRecord patientRecord : patientRecordMap.values()) {
            System.out.printf("|%-12s| %-25s|\n", patientRecord.getName(), patientRecord.getHealthProduct());
            printTableRowBorder("-", 39);
        }
        printTableRowBorder("=", 39);
        System.out.println();

        displayOrderedHealthProduct();
    }

    /**
     * Display Patient record
     *
     * @param patientRecord
     */
    @Override
    public void displayPatientRecord(PatientRecord patientRecord) {
        printTableHeaderForLastUpdatedPatientRecord();
        System.out.printf("|%-12s| %-14s| %-15s| %-17s| %-50s|\n",
                patientRecord.getName(),
                patientRecord.getContactNumber(),
                patientRecord.getPhysiotherapist().getName(),
                patientRecord.getHealthProduct(),
                patientRecord.getNote());
        printTableRowBorder("=", 116);
    }

    /**
     * Get patient record by name and number
     *
     * @param name
     * @param contactNumber
     * @return
     * @throws Exception
     */
    @Override
    public PatientRecord getPatientRecordByNameAndNumber(String name, String contactNumber) throws Exception {

        PatientRecord patientRecordResult = null;
        for (PatientRecord patientRecord : patientRecordMap.values()) {
            if (patientRecord.getName().equals(name) && patientRecord.getContactNumber().equals(contactNumber)) {
                patientRecordResult = patientRecord;
            }
        }

        if (Objects.isNull(patientRecordResult)) {
            throw new Exception("No Patient Record available for given name and contact number");
        }

        return patientRecordResult;
    }

    /**
     * Get Patient Record map
     *
     * @return
     */
    @Override
    public Map<Integer, PatientRecord> getPatientRecordMap() {
        return patientRecordMap;
    }
}
