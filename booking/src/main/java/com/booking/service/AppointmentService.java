package com.booking.service;


import com.booking.model.dao.Appointment;
import com.booking.model.dao.PatientRecord;
import com.booking.model.enums.HealthService;

import java.time.LocalDateTime;


public interface AppointmentService {

    /**
     * add Appointment to appointmentMap
     *
     * @param appointment
     */
    void addAppointment(Appointment appointment);

    /**
     * Book Appointment
     *
     * @param patientRecord
     * @param appointmentDate
     * @param healthService
     * @throws Exception
     */
    void bookAppointment(PatientRecord patientRecord, LocalDateTime appointmentDate, HealthService healthService) throws Exception;

    /**
     * Chnage Appointment
     *
     * @param appointment
     * @param appointmentDate
     */
    void changeAppointmentDate(Appointment appointment, LocalDateTime appointmentDate);

    /**
     * Cancel Appointment
     *
     * @param appointment
     * @throws Exception
     */
    void cancelAppointment(Appointment appointment) throws Exception;

    /**
     * Display last month appointment
     *
     * @param patientRecordService
     * @throws Exception
     */
    void displayLastMonthAppointments(PatientRecordService patientRecordService) throws Exception;

    /**
     * Display Appointment
     *
     * @param appointment
     */
    void displayAppointment(Appointment appointment);

    /**
     * Get Appointment
     *
     * @param appointmentId
     * @return
     */
    Appointment getAppointment(int appointmentId);

    /**
     * Display Appointment by patient record
     *
     * @param patientRecord
     */
    void displayAppointmentByPatientRecord(PatientRecord patientRecord);

    /**
     * Check that is appointment date available or not
     *
     * @param appointmentDate
     * @param physiotherapistId
     * @return
     */
    Boolean isAppointmentDateAvailable(LocalDateTime appointmentDate, int physiotherapistId);

    /**
     * Check in Patient
     *
     * @param appointment
     * @return
     */
    Boolean checkInPatient(Appointment appointment);

}
