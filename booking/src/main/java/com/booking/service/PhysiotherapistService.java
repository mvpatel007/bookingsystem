package com.booking.service;


import com.booking.model.dao.Physiotherapist;

public interface PhysiotherapistService {

    /**
     * add physiotherapist
     *
     * @param physiotherapist
     * @throws Exception
     */
    public void addPhysiotherapist(Physiotherapist physiotherapist) throws Exception;

    /**
     * get physiotherapist
     *
     * @param physiotherapistId
     * @return
     * @throws Exception
     */
    public Physiotherapist getPhysiotherapist(int physiotherapistId) throws Exception;
}