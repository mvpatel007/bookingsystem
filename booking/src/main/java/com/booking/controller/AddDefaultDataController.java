package com.booking.controller;

import com.booking.model.dao.Appointment;
import com.booking.model.dao.PatientRecord;
import com.booking.model.dao.Physiotherapist;
import com.booking.model.enums.AppointmentStatus;
import com.booking.model.enums.HealthProduct;
import com.booking.model.enums.HealthService;
import com.booking.service.AppointmentService;
import com.booking.service.PatientRecordService;
import com.booking.service.PhysiotherapistService;

import java.time.LocalDateTime;

/**
 * This class is responsible to add all default data.
 * Adding default data for physiotherapist, patient record and appointments.
 */
public class AddDefaultDataController {

    private PhysiotherapistService physiotherapistService;
    private PatientRecordService patientRecordService;
    private AppointmentService appointmentService;

    public AddDefaultDataController(final PhysiotherapistService physiotherapistService, final PatientRecordService patientRecordService, final AppointmentService appointmentService) {
        this.physiotherapistService = physiotherapistService;
        this.patientRecordService = patientRecordService;
        this.appointmentService = appointmentService;
        addDefaultData();
    }

    /**
     * Set up Default Set Default Physiotherapist List
     */
    private void addDefaultData() {

        try {

            Physiotherapist physiotherapist1 = new Physiotherapist("Emelia");
            Physiotherapist physiotherapist2 = new Physiotherapist("Dolores");
            Physiotherapist physiotherapist3 = new Physiotherapist("Susannah");

            physiotherapistService.addPhysiotherapist(physiotherapist1);
            physiotherapistService.addPhysiotherapist(physiotherapist2);
            physiotherapistService.addPhysiotherapist(physiotherapist3);

            PatientRecord patientRecord1 = new PatientRecord("Johan", "0123456789", "Note 1", physiotherapist1, HealthProduct.PAIN_RELIEF);
            PatientRecord patientRecord2 = new PatientRecord("Kathryn", "07912312313", "Note 1", physiotherapist1, HealthProduct.PAIN_RELIEF);
            PatientRecord patientRecord3 = new PatientRecord("Claudius", "07912312314", "Note 1", physiotherapist1, HealthProduct.INJECTION_THERAPY);
            PatientRecord patientRecord4 = new PatientRecord("Mari", "07912312315", "Note 1", physiotherapist2, HealthProduct.NAPROXE);
            PatientRecord patientRecord5 = new PatientRecord("Clio", "07912312316", "Note 1", physiotherapist2, HealthProduct.PAIN_RELIEF);
            PatientRecord patientRecord6 = new PatientRecord("Kelley", "07912312317", "Note 1", physiotherapist2, HealthProduct.NORCO);
            PatientRecord patientRecord7 = new PatientRecord("Salazar", "07912312318", "Note 1", physiotherapist2, HealthProduct.IBUPROFEN);
            PatientRecord patientRecord8 = new PatientRecord("Santiago", "07912312319", "Note 1", physiotherapist3, HealthProduct.ANTIBIOTIC);
            PatientRecord patientRecord9 = new PatientRecord("Wilson", "07912312310", "Note 1", physiotherapist3, HealthProduct.ANTIBIOTIC);
            PatientRecord patientRecord10 = new PatientRecord("Craig", "07912312311", "Note 1", physiotherapist3, HealthProduct.NAPROX);

            patientRecordService.addPatientRecord(patientRecord1);
            patientRecordService.addPatientRecord(patientRecord2);
            patientRecordService.addPatientRecord(patientRecord3);
            patientRecordService.addPatientRecord(patientRecord4);
            patientRecordService.addPatientRecord(patientRecord5);
            patientRecordService.addPatientRecord(patientRecord6);
            patientRecordService.addPatientRecord(patientRecord7);
            patientRecordService.addPatientRecord(patientRecord8);
            patientRecordService.addPatientRecord(patientRecord9);
            patientRecordService.addPatientRecord(patientRecord10);


            LocalDateTime date1 = LocalDateTime.of(2018, 12, 28, 10, 00);
            LocalDateTime date2 = LocalDateTime.of(2018, 12, 29, 11, 30);
            LocalDateTime date3 = LocalDateTime.of(2018, 12, 30, 12, 00);
            LocalDateTime date4 = LocalDateTime.of(2018, 12, 03, 13, 30);
            LocalDateTime date5 = LocalDateTime.of(2018, 12, 04, 14, 00);
            LocalDateTime date6 = LocalDateTime.of(2018, 12, 05, 15, 30);
            LocalDateTime date7 = LocalDateTime.of(2018, 12, 06, 16, 00);
            LocalDateTime date8 = LocalDateTime.of(2018, 12, 07, 17, 00);
            LocalDateTime date9 = LocalDateTime.of(2018, 12, 10, 10, 00);
            LocalDateTime date10 = LocalDateTime.of(2018, 12, 11, 11, 30);
            LocalDateTime date11 = LocalDateTime.of(2018, 12, 12, 12, 00);
            LocalDateTime date12 = LocalDateTime.of(2018, 12, 13, 13, 30);
            LocalDateTime date13 = LocalDateTime.of(2018, 12, 14, 14, 00);
            LocalDateTime date14 = LocalDateTime.of(2018, 12, 17, 15, 30);
            LocalDateTime date15 = LocalDateTime.of(2018, 11, 18, 16, 00);
            LocalDateTime date16 = LocalDateTime.of(2018, 11, 28, 10, 00);
            LocalDateTime date17 = LocalDateTime.of(2018, 11, 29, 11, 30);
            LocalDateTime date18 = LocalDateTime.of(2018, 11, 30, 12, 00);
            LocalDateTime date19 = LocalDateTime.of(2018, 11, 03, 13, 30);
            LocalDateTime date20 = LocalDateTime.of(2018, 11, 04, 14, 00);
            LocalDateTime date21 = LocalDateTime.of(2018, 11, 05, 15, 30);
            LocalDateTime date22 = LocalDateTime.of(2018, 11, 06, 16, 00);
            LocalDateTime date23 = LocalDateTime.of(2018, 11, 07, 17, 00);
            LocalDateTime date24 = LocalDateTime.of(2018, 11, 10, 10, 00);
            LocalDateTime date25 = LocalDateTime.of(2018, 11, 11, 11, 30);
            LocalDateTime date26 = LocalDateTime.of(2018, 11, 12, 12, 00);
            LocalDateTime date27 = LocalDateTime.of(2018, 11, 13, 13, 30);
            LocalDateTime date28 = LocalDateTime.of(2018, 10, 14, 14, 00);
            LocalDateTime date29 = LocalDateTime.of(2018, 11, 17, 15, 30);
            LocalDateTime date30 = LocalDateTime.of(2018, 11, 18, 16, 00);

            Appointment appointment1 = new Appointment(patientRecord1, date1, HealthService.ACUPUNCTURE, AppointmentStatus.BOOKED);
            Appointment appointment2 = new Appointment(patientRecord2, date2, HealthService.ACUPUNCTURE, AppointmentStatus.BOOKED);
            Appointment appointment3 = new Appointment(patientRecord3, date3, HealthService.ACUPUNCTURE, AppointmentStatus.BOOKED);
            Appointment appointment4 = new Appointment(patientRecord4, date4, HealthService.ACUPUNCTURE, AppointmentStatus.BOOKED);
            Appointment appointment5 = new Appointment(patientRecord5, date5, HealthService.ACUPUNCTURE, AppointmentStatus.CANCELED);
            Appointment appointment6 = new Appointment(patientRecord6, date6, HealthService.MASSAGE, AppointmentStatus.CANCELED);
            Appointment appointment7 = new Appointment(patientRecord7, date7, HealthService.MASSAGE, AppointmentStatus.CANCELED);
            Appointment appointment8 = new Appointment(patientRecord8, date8, HealthService.MASSAGE, AppointmentStatus.ATTENDED);
            Appointment appointment9 = new Appointment(patientRecord9, date9, HealthService.MASSAGE, AppointmentStatus.ATTENDED);
            Appointment appointment10 = new Appointment(patientRecord10, date10, HealthService.MASSAGE, AppointmentStatus.ATTENDED);
            Appointment appointment11 = new Appointment(patientRecord1, date11, HealthService.MOBILISATION_OF_SPINE, AppointmentStatus.BOOKED);
            Appointment appointment12 = new Appointment(patientRecord2, date12, HealthService.MOBILISATION_OF_SPINE, AppointmentStatus.BOOKED);
            Appointment appointment13 = new Appointment(patientRecord3, date13, HealthService.MOBILISATION_OF_SPINE, AppointmentStatus.BOOKED);
            Appointment appointment14 = new Appointment(patientRecord4, date14, HealthService.NEURAL_MOBILISATION, AppointmentStatus.CANCELED);
            Appointment appointment15 = new Appointment(patientRecord5, date15, HealthService.NEURAL_MOBILISATION, AppointmentStatus.BOOKED);

            Appointment appointment16 = new Appointment(patientRecord1, date16, HealthService.ACUPUNCTURE, AppointmentStatus.BOOKED);
            Appointment appointment17 = new Appointment(patientRecord2, date17, HealthService.ACUPUNCTURE, AppointmentStatus.BOOKED);
            Appointment appointment18 = new Appointment(patientRecord3, date18, HealthService.ACUPUNCTURE, AppointmentStatus.BOOKED);
            Appointment appointment19 = new Appointment(patientRecord4, date19, HealthService.ACUPUNCTURE, AppointmentStatus.CANCELED);
            Appointment appointment20 = new Appointment(patientRecord5, date20, HealthService.ACUPUNCTURE, AppointmentStatus.CANCELED);
            Appointment appointment21 = new Appointment(patientRecord6, date21, HealthService.MASSAGE, AppointmentStatus.CANCELED);
            Appointment appointment22 = new Appointment(patientRecord7, date22, HealthService.MASSAGE, AppointmentStatus.CANCELED);
            Appointment appointment23 = new Appointment(patientRecord8, date23, HealthService.MASSAGE, AppointmentStatus.ATTENDED);
            Appointment appointment24 = new Appointment(patientRecord9, date24, HealthService.MASSAGE, AppointmentStatus.ATTENDED);
            Appointment appointment25 = new Appointment(patientRecord10, date25, HealthService.MASSAGE, AppointmentStatus.ATTENDED);
            Appointment appointment26 = new Appointment(patientRecord1, date26, HealthService.MOBILISATION_OF_SPINE, AppointmentStatus.BOOKED);
            Appointment appointment27 = new Appointment(patientRecord2, date27, HealthService.MOBILISATION_OF_SPINE, AppointmentStatus.BOOKED);
            Appointment appointment28 = new Appointment(patientRecord3, date28, HealthService.MOBILISATION_OF_SPINE, AppointmentStatus.MISSED);
            Appointment appointment29 = new Appointment(patientRecord4, date29, HealthService.NEURAL_MOBILISATION, AppointmentStatus.CANCELED);
            Appointment appointment30 = new Appointment(patientRecord5, date30, HealthService.NEURAL_MOBILISATION, AppointmentStatus.BOOKED);

            appointmentService.addAppointment(appointment1);
            appointmentService.addAppointment(appointment2);
            appointmentService.addAppointment(appointment3);
            appointmentService.addAppointment(appointment4);
            appointmentService.addAppointment(appointment5);
            appointmentService.addAppointment(appointment6);
            appointmentService.addAppointment(appointment7);
            appointmentService.addAppointment(appointment8);
            appointmentService.addAppointment(appointment9);
            appointmentService.addAppointment(appointment10);
            appointmentService.addAppointment(appointment11);
            appointmentService.addAppointment(appointment12);
            appointmentService.addAppointment(appointment13);
            appointmentService.addAppointment(appointment14);
            appointmentService.addAppointment(appointment15);
            appointmentService.addAppointment(appointment16);
            appointmentService.addAppointment(appointment17);
            appointmentService.addAppointment(appointment18);
            appointmentService.addAppointment(appointment19);
            appointmentService.addAppointment(appointment20);
            appointmentService.addAppointment(appointment21);
            appointmentService.addAppointment(appointment22);
            appointmentService.addAppointment(appointment23);
            appointmentService.addAppointment(appointment24);
            appointmentService.addAppointment(appointment25);
            appointmentService.addAppointment(appointment26);
            appointmentService.addAppointment(appointment27);
            appointmentService.addAppointment(appointment28);
            appointmentService.addAppointment(appointment29);
            appointmentService.addAppointment(appointment30);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
