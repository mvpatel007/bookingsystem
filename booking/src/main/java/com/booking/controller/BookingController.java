package com.booking.controller;


import com.booking.model.dao.Appointment;
import com.booking.model.dao.PatientRecord;
import com.booking.model.enums.HealthProduct;
import com.booking.model.enums.HealthService;
import com.booking.service.AppointmentService;
import com.booking.service.PatientRecordService;
import com.booking.service.PhysiotherapistService;
import com.booking.service.utils.DateUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


/**
 * BookingController responsible for booking functionalities
 */
public class BookingController {

    private Scanner scanner;
    private PhysiotherapistService physiotherapistService;
    private PatientRecordService patientRecordService;
    private AppointmentService appointmentService;
    private List<HealthService> healthServiceList = Arrays.asList(HealthService.values());
    private List<HealthProduct> healthProductList = Arrays.asList(HealthProduct.values());


    /**
     * @param scanner
     * @param physiotherapistService
     * @param patientRecordService
     * @param appointmentService
     */
    public BookingController(final Scanner scanner,
                             final PhysiotherapistService physiotherapistService,
                             final PatientRecordService patientRecordService,
                             final AppointmentService appointmentService) {
        this.scanner = scanner;
        this.physiotherapistService = physiotherapistService;
        this.patientRecordService = patientRecordService;
        this.appointmentService = appointmentService;
    }

    /**
     * Responsible for book appointment
     */
    public void bookAppointment() {
        try {
            PatientRecord patientRecord = patientRecordService.getPatientRecordByNameAndNumber(getPatientNameByScanner(), getPatientContactNumberByScanner());

            LocalDateTime appointmentDate = DateUtils.getDateByScanner(scanner);
            appointmentService.bookAppointment(patientRecord, appointmentDate, getPatientHealthServiceByScanner());

        } catch (Exception e) {
            System.out.println(e.getMessage());
            bookAppointment();
        }
    }


    /**
     * Get patient name by Scanner
     *
     * @return
     */
    public String getPatientNameByScanner() {
        try {
            System.out.println("Give Patient Name, i.e. Johan");
            String patientName = scanner.next();
            return patientName;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return getPatientNameByScanner();
        }
    }

    /**
     * Get Patient contact number by scanner
     *
     * @return
     */
    public String getPatientContactNumberByScanner() {
        try {
            System.out.println("Give Patient contact number, i.e. 0123456789");
            String patientContactNumber = scanner.next();
            return patientContactNumber;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return getPatientContactNumberByScanner();
        }
    }

    /**
     * Get Appointment Id by scanner
     *
     * @return
     */
    public int getAppointmentIdByScanner() {
        try {
            PatientRecord patientRecord = patientRecordService.getPatientRecordByNameAndNumber(getPatientNameByScanner(), getPatientContactNumberByScanner());
            appointmentService.displayAppointmentByPatientRecord(patientRecord);
            System.out.println("Give Appointment id, i.e. 3011 or 3016");
            return scanner.nextInt();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            scanner.next();
            return getAppointmentIdByScanner();
        }
    }

    /**
     * Get Patient Health Service by Scanner
     *
     * @return
     */
    public HealthService getPatientHealthServiceByScanner() {
        try {
            System.out.println(String.format("Give required HealthService From below List, i.e. MUSCLE_STRETCHING\n"));
            healthServiceList.forEach(System.out::println);
            HealthService healthService = HealthService.valueOf(scanner.next());
            return healthService;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return getPatientHealthServiceByScanner();
        }
    }

    /**
     * Get Health product by scanner
     *
     * @return
     */
    public HealthProduct getHealthProductByScanner() {
        try {
            System.out.println(String.format("Select Health product from below list, i.e. TRAMADOL_HCL\n"));
            healthProductList.forEach(System.out::println);
            HealthProduct healthProduct = HealthProduct.valueOf(scanner.next());
            return healthProduct;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return getHealthProductByScanner();
        }
    }

    /**
     * Get Note by scanner
     *
     * @return
     */
    public String getNoteByScanner() {
        try {
            System.out.println("Give note for the patient record, i.e. Exercise treatment is recommended");
            String note = scanner.next();
            note += scanner.nextLine();
            return note;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return getNoteByScanner();
        }
    }

    /**
     * Chnage Appointment date
     */
    public void changeAppointmentDate() {

        try {
            int appointmentId = getAppointmentIdByScanner();
            LocalDateTime appointmentDate = DateUtils.getDateByScanner(scanner);
            Appointment appointment = appointmentService.getAppointment(appointmentId);
            appointmentService.changeAppointmentDate(appointment, appointmentDate);
        } catch (Exception e) {
            scanner.next();
            System.out.println(e.getMessage());
            changeAppointmentDate();
        }
    }

    /**
     * Cancel Appointment
     */
    public void cancelAppointment() {
        try {
            Appointment appointment = getAppointmentByAppointmentId(getAppointmentIdByScanner());
            appointmentService.cancelAppointment(appointment);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            cancelAppointment();
        }
    }

    /**
     * Check in Patient
     */
    public void checkInPatient() {
        try {
            int appointmentId = getAppointmentIdByScanner();
            Appointment appointment = getAppointmentByAppointmentId(appointmentId);

            if (appointmentService.checkInPatient(appointment)) {

                HealthProduct healthProduct = getHealthProductByScanner();
                String note = getNoteByScanner();
                PatientRecord patientRecord = appointment.getPatientRecord();
                patientRecord.setNote(note);
                patientRecord.setHealthProduct(healthProduct);

                System.out.println("Patient is checked in, given note and health product is updated in patient " +
                        "record check details for updated Appointment and Patient Record list below \n");
                appointmentService.displayAppointment(appointment);
                System.out.println();
                patientRecordService.displayPatientRecord(appointment.getPatientRecord());
            } else {
                System.out.println("Selected Appointment is past or already checked in or canceled");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            scanner.next();
            checkInPatient();
        }
    }

    /**
     *
     */
    public void displayLastMonthAppointments() {
        try {

            appointmentService.displayLastMonthAppointments(patientRecordService);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * get Appointment by appointment id
     *
     * @param appointmentId
     * @return
     * @throws NullPointerException
     */
    private Appointment getAppointmentByAppointmentId(int appointmentId) throws NullPointerException {
        try {
            Appointment appointment = appointmentService.getAppointment(appointmentId);
            if (appointment == null) {
                throw new NullPointerException("Given Appointment id is not valid1");
            }
            return appointment;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Display current health product
     */
    public void displayCurrentHealthProduct() {
        try {

            patientRecordService.displayCurrentHealthProduct();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}