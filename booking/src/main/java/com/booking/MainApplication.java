package com.booking;

import com.booking.controller.AddDefaultDataController;
import com.booking.controller.BookingController;
import com.booking.service.AppointmentService;
import com.booking.service.PatientRecordService;
import com.booking.service.PhysiotherapistService;
import com.booking.service.impl.AppointmentServiceImpl;
import com.booking.service.impl.PatientRecordServiceImpl;
import com.booking.service.impl.PhysiotherapistServiceImpl;

import java.util.Scanner;

/**
 * Main Application class to run this project.
 */
public class MainApplication {

    private Scanner scanner = new Scanner(System.in);
    private PhysiotherapistService physiotherapistService = new PhysiotherapistServiceImpl();
    private PatientRecordService patientRecordService = new PatientRecordServiceImpl();
    private AppointmentService appointmentService = new AppointmentServiceImpl();

    private AddDefaultDataController addDefaultDataController =
            new AddDefaultDataController(physiotherapistService, patientRecordService, appointmentService);

    private BookingController bookingController =
            new BookingController(scanner, physiotherapistService, patientRecordService, appointmentService);

    /**
     * Main method to run this application
     *
     * @param args
     */
    public static void main(String[] args) {
        MainApplication mainApplication = new MainApplication();
        mainApplication.selectOption();
    }

    /**
     * Select Option Method
     * This method will call different methods depends on given input
     */
    private void selectOption() {
        try {
            viewOptionList();

            int optionNumber = scanner.nextInt();

            switch (optionNumber) {
                case 1:
                    bookingController.bookAppointment();
                    break;
                case 2:
                    bookingController.changeAppointmentDate();
                    break;
                case 3:
                    bookingController.cancelAppointment();
                    break;
                case 4:
                    bookingController.checkInPatient();
                    break;
                case 5:
                    bookingController.displayLastMonthAppointments();
                    break;
                case 6:
                    bookingController.displayCurrentHealthProduct();
                    break;
                default:
                    wrongOption();
            }
            // call back method - selectOption()
            selectOption();
        } catch (Exception e) {
            scanner.next();
            System.out.println(e.getMessage());
            wrongOption();
        }
    }


    /**
     * View Option List
     */
    private void viewOptionList() {

        String display = "";
        display += "Choose required option from below list\n";
        display += "1. Book Appointment\n";
        display += "2. Change Appointment Date\n";
        display += "3. Cancel Appointment\n";
        display += "4. Check In Patient, Update note and product in patient record\n";
        display += "5. Show Appointments Record for last month\n";
        display += "6. Show Health Product report\n";
        System.out.println(display);
    }

    /**
     * Wrong Option
     */
    private void wrongOption() {
        System.out.println("Input is not valid, Input must be from given options");
        selectOption();
    }
}
