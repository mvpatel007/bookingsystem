package com.booking.service.impl;

import com.booking.model.dao.Appointment;
import com.booking.model.dao.PatientRecord;
import com.booking.model.dao.Physiotherapist;
import com.booking.model.enums.AppointmentStatus;
import com.booking.model.enums.HealthProduct;
import com.booking.model.enums.HealthService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

/**
 * test for check in patient
 */
public class checkInPatientTest extends AppointmentServiceImpl {

    Appointment appointment1;
    Appointment appointment2;
    Appointment appointment3;
    Appointment appointment4;

    /**
     * @throws Exception mainApplication is holding appointmentService
     *                   to check time for already booked appointment default data should be added first.
     *                   To add default data object of mainApplication should be created.
     */
    @Before
    public void setUp() {
        Physiotherapist physiotherapist = new Physiotherapist("Emelia");

        PatientRecord patientRecord1 = new PatientRecord("Jalil", "07912312312", "Note 1", physiotherapist, HealthProduct.PAIN_RELIEF);

        LocalDateTime date1 = LocalDateTime.of(2019, 2, 28, 10, 00);
        LocalDateTime date2 = LocalDateTime.of(2019, 2, 20, 10, 00);
        LocalDateTime date3 = LocalDateTime.of(2019, 2, 21, 10, 00);
        LocalDateTime date4 = LocalDateTime.of(2019, 2, 21, 10, 00);

        appointment1 = new Appointment(patientRecord1, date1, HealthService.ACUPUNCTURE, AppointmentStatus.BOOKED);
        appointment2 = new Appointment(patientRecord1, date2, HealthService.ACUPUNCTURE, AppointmentStatus.CANCELED);
        appointment3 = new Appointment(patientRecord1, date3, HealthService.ACUPUNCTURE, AppointmentStatus.ATTENDED);
        appointment4 = new Appointment(patientRecord1, date4, HealthService.ACUPUNCTURE, AppointmentStatus.MISSED);

        this.addAppointment(appointment1);
    }

    @After
    public void tearDown() {
    }

    /**
     * check that allow to do check in if appointment status is booked
     */
    @Test
    public void checkInPatientWhenAppointmentStatusBookedTest() {
        try {

            Boolean actual = this.checkInPatient(appointment1);
            assertTrue(actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * check that not allow to check in if appointment status is cancelled
     */
    @Test
    public void checkInPatientWhenAppointmentStatusCancelledTest() {
        try {

            Boolean actual = this.checkInPatient(appointment2);
            assertFalse(actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * check that not allow to check in if appointment status is Attended
     */
    @Test
    public void checkInPatientWhenAppointmentStatusAttendedTest() {
        try {

            Boolean actual = this.checkInPatient(appointment3);
            assertFalse(actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * check that after check in appointment status is changed to attended
     */
    @Test
    public void checkInPatientAfterBookAppointmentStatusAttendedTest() {
        try {

            this.checkInPatient(appointment1);
            AppointmentStatus expected = AppointmentStatus.ATTENDED;
            AppointmentStatus actual = appointment1.getAppointmentStatus();

            assertEquals(expected, actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * check that if check in is not successful then appointment status should not be changed
     */
    @Test
    public void checkInPatientAfterBookAppointmentStatusSameAsBeforeTest() {
        try {
            AppointmentStatus expected = appointment4.getAppointmentStatus();
            this.checkInPatient(appointment4);
            AppointmentStatus actual = appointment4.getAppointmentStatus();

            assertEquals(expected, actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}