package com.booking.service.impl;

import com.booking.model.dao.Appointment;
import com.booking.model.dao.PatientRecord;
import com.booking.model.dao.Physiotherapist;
import com.booking.model.enums.AppointmentStatus;
import com.booking.model.enums.HealthProduct;
import com.booking.model.enums.HealthService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for IsAppointmentAvailableTest class
 */
public class IsAppointmentAvailableTest extends AppointmentServiceImpl {

    LocalDateTime appointmentDate;
    Physiotherapist physiotherapist;

    /**
     * @throws Exception
     */
    @Before
    public void setUp() {
        physiotherapist = new Physiotherapist("Emelia");

        PatientRecord patientRecord1 = new PatientRecord("Jalil", "07912312312", "Note 1", physiotherapist, HealthProduct.PAIN_RELIEF);

        LocalDateTime date1 = LocalDateTime.of(2018, 12, 28, 10, 00);

        Appointment appointment1 = new Appointment(patientRecord1, date1, HealthService.ACUPUNCTURE, AppointmentStatus.BOOKED);

        this.addAppointment(appointment1);
    }

    @After
    public void tearDown() {
    }

    /**
     * Checking that that past date appointment is not available to book
     */
    @Test
    public void forPastDateTest() {
        try {
            appointmentDate = LocalDateTime.of(2018, 11, 28, 10, 00);

            Boolean actual = this.isAppointmentDateAvailable(appointmentDate, physiotherapist.getPhysiotherapistId());
            assertFalse(actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Checking that out of available hours in early morning appointment is not available to book.
     */
    @Test
    public void notAvailableEarlyMorningTimeTest() {
        try {

            appointmentDate = LocalDateTime.of(2018, 11, 28, 6, 00);

            Boolean actual = this.isAppointmentDateAvailable(appointmentDate, physiotherapist.getPhysiotherapistId());
            assertFalse(actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Checking that out of available hours in late night appointment is not able to book
     */
    @Test
    public void notAvailableLateNightTimeTest() {
        try {
            appointmentDate = LocalDateTime.of(2018, 11, 28, 23, 00);

            Boolean actual = this.isAppointmentDateAvailable(appointmentDate, physiotherapist.getPhysiotherapistId());
            assertFalse(actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    /**
     * Checking that already booked appointment time is not able to book
     */
    @Test
    public void alreadyBookedTimeTest() {
        try {

            appointmentDate = LocalDateTime.of(2018, 12, 28, 10, 00);

            Boolean actual = this.isAppointmentDateAvailable(appointmentDate, physiotherapist.getPhysiotherapistId());
            assertFalse(actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * As assumption  each appointment is available at each 30 minutes.
     * Checking that appointment is not available to book if minutes is not 0 or 30
     */
    @Test
    public void notAvailableMinuteTimeTest() {
        try {
            appointmentDate = LocalDateTime.of(2018, 12, 18, 16, 22);

            Boolean actual = this.isAppointmentDateAvailable(appointmentDate, physiotherapist.getPhysiotherapistId());
            assertFalse(actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Check that appointment can be booked if given timing and physiotherapist id is appropriate.
     */
    @Test
    public void checkForAvailableTime() {
        try {

            appointmentDate = LocalDateTime.of(2019, 2, 10, 16, 30);

            Boolean actual = this.isAppointmentDateAvailable(appointmentDate, physiotherapist.getPhysiotherapistId());
            assertTrue(actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}